# coding:utf-8
# 引入app配置
import os

import pymysql
from flask import jsonify  # JSON
from flask import render_template  # 读取页面
from flask import send_from_directory

from application import app, conn, cache  # 引入app

pymysql.install_as_MySQLdb()


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


def connect_mysql(conn):
    # 判断链接是否正常
    conn.ping(True)
    # 建立操作游标
    cursor = conn.cursor()
    # 设置数据输入输出编码格式
    cursor.execute('set names utf8')
    return cursor


# 首页
@app.route('/')
def index():
    return render_template('index.html')


# 地点薪资图页面(地图)
# 图表形式参考链接：http://echarts.baidu.com/demo.html#map-china-dataRange
@app.route('/zwyx/dd_index')
def zwyx_dd_view():
    return render_template('zwyx_dd.html')


# 地点和薪资
@app.route('/zwyx/dd')
def show_zwyx_dd():
    rv = cache.get('zwyx_dd')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        # 初始化返回的字典
        returnDate = {}
        returnDate['status'] = 0
        # 查询地点和薪资的关系，职位总数，平均薪资
        sql = "select avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_dd.province,zp_dd.id from zp_list inner join zp_dd on zp_dd.Id=zp_list.dd_id where province is not NULL and zp_list.min_zwyx!=0 group by province"
        # 执行sql语句
        cursor.execute(sql)
        # 取出所有结果集
        dd_zwyx_list = cursor.fetchall()
        # 平均薪资
        avg_zwyx = {}
        # 总职位数
        count_zw = {}
        if dd_zwyx_list:
            # 循环遍历重新构建数据
            for value in dd_zwyx_list:
                # 取得地点名
                dd_name = value[1]
                # 判断平均薪资数据录入
                count_zw[dd_name] = {'name': dd_name, 'value': round(value[0], 2)}
            # 重新构建数据
            return_avg_zwyx = count_zw.values()
            # 数据汇总
            returnDate['avg_zwyx'] = return_avg_zwyx
            returnDate['status'] = 1
        # 关闭游标链接
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_dd', rv, timeout=100 * 60)
    return rv


# 学历薪资图页面（柱状图）
# 参考链接:http://echarts.baidu.com/demo.html#bar-stack
@app.route('/zwyx/xl_index')
def zwyx_xl_view():
    return render_template('zwyx_xl.html')


# 学历和薪资
@app.route('/zwyx/xl')
def show_zwyx_xl():
    rv = cache.get('zwyx_xl')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        # 设置数据输入输出编码格式
        cursor.execute('set names utf8')
        returnDate = {}
        returnDate['status'] = 0
        returnDate['data'] = {}
        # 查询地点和薪资的关系，职位总数，平均薪资
        sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_xl.xl_name,max(max_zwyx),min(max_zwyx) from zp_list inner join zp_xl on zp_xl.Id=zp_list.xl_id where min_zwyx!=0 group by xl_id order by count_zw desc'
        # 执行sql语句
        cursor.execute(sql)
        # 取出所有结果集
        xl_zwyx_list = cursor.fetchall()
        if xl_zwyx_list:
            returnDate['status'] = 1
            # 总职位数
            count_zw = []
            # 平均薪资
            avg_zw = []
            # 学历名
            xl_list = []
            # 最大薪资
            max_xz = []
            # 最小薪资
            min_xz = []
            # 循环遍历存入数据
            for item in xl_zwyx_list:
                count_zw.append(item[0])
                avg_zw.append(float(round(item[1], 2)))  # 精度保留2位小数
                xl_list.append(item[2])
                max_xz.append(int(item[3]))
                min_xz.append(int(item[4]))
            # 数据json化
            returnDate['count_zw'] = count_zw
            returnDate['avg_zw'] = avg_zw
            returnDate['xl_list'] = xl_list
            returnDate['max_xz'] = max_xz
            returnDate['min_xz'] = min_xz
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_xl', rv, timeout=100 * 60)
    return rv


# 公司规模薪资图页面（饼图）
# 参考链接：http://echarts.baidu.com/demo.html#pie-roseType
@app.route('/zwyx/gsgm_index')
def zwyx_gsgm_view():
    return render_template('zwyx_gsgm.html')


# 公司规模和薪资（饼图）
@app.route('/zwyx/gsgm')
def show_zwyx_gsgm():
    rv = cache.get('zwyx_gsgm')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        returnDate['status'] = 0
        # 查询地点和薪资的关系，职位总数，平均薪资
        sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_gsgm.gsgm_name from zp_list inner join zp_gsgm on zp_gsgm.Id=zp_list.gsgm_id where min_zwyx!=0 group by gsgm_id order by count_zw desc'
        # 执行sql语句
        cursor.execute(sql)
        # 取出所有结果集
        gsgm_zwyx_list = cursor.fetchall()
        if gsgm_zwyx_list:
            returnDate['status'] = 1
            # 总职位数
            count_zw = []
            # 平均薪资
            avg_zw = []
            # 公司规模名
            gsgm_list = []
            # 循环遍历存入数据
            for item in gsgm_zwyx_list:
                count_zw.append({'name': item[2], 'value': item[0]})
                avg_zw.append({'name': item[2], 'value': round(item[1], 2)})
                gsgm_list.append(item[2])
            returnDate['count_zw'] = count_zw
            returnDate['avg_zw'] = avg_zw
            returnDate['gsgm_list'] = gsgm_list
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_gsgm', rv, timeout=100 * 60)
    return rv


# 公司性质薪资图页面（折线图）
# 参考链接：http://echarts.baidu.com/demo.html#line-marker
@app.route('/zwyx/gsxz_index')
def zwyx_gsxz_view():
    return render_template('zwyx_gsxz.html')


# 公司性质和薪资
@app.route('/zwyx/gsxz')
def show_zwyx_gsxz():
    rv = cache.get('zwyx_gsxz')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        # 查询地点和薪资的关系，职位总数，平均薪资
        sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_gsxz.gsxz_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_gsxz on zp_gsxz.Id=zp_list.gsxz_id where min_zwyx!=0 group by gsxz_id order by count_zw desc'
        # 执行sql语句
        cursor.execute(sql)
        # 取出所有结果集
        gsxz_zwyx_list = cursor.fetchall()
        if gsxz_zwyx_list:
            returnDate['status'] = 1
            # 总职位数
            count_zw = []
            # 平均薪资
            avg_zw = []
            # 公司规模名
            gsxz_list = []
            # 最大薪资
            max_xz = []
            # 最小薪资
            min_xz = []
            # 循环遍历存入数据
            for item in gsxz_zwyx_list:
                count_zw.append({'name': item[2], 'value': item[0]})
                avg_zw.append({'name': item[2], 'value': round(item[1], 2)})
                gsxz_list.append(item[2])
                max_xz.append(int(item[3]))
                min_xz.append(int(item[4]))
            # 数据json化
            returnDate['count_zw'] = count_zw
            returnDate['avg_zw'] = avg_zw
            returnDate['gsxz_list'] = gsxz_list
            returnDate['max_xz'] = max_xz
            returnDate['min_xz'] = min_xz
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_gsxz', rv, timeout=100 * 60)
    return rv


# 经验薪资图页面（雷达图）
# 参考链接：http://echarts.baidu.com/demo.html#radar-custom
@app.route('/zwyx/jy_index')
def zwyx_jy_view():
    return render_template('zwyx_jy.html')


# 经验和薪资
@app.route('/zwyx/jy')
def show_zwyx_jy():
    rv = cache.get('zwyx_jy')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        returnDate['status'] = 0
        sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_jy.jy_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_jy on zp_jy.Id=zp_list.jy_id where min_zwyx!=0 group by jy_id order by count_zw desc'
        # 执行sql语句
        cursor.execute(sql)
        # 取出所有结果集
        jy_zwyx_list = cursor.fetchall()
        if jy_zwyx_list:
            # 总职位数
            count_zw = []
            # 平均薪资
            avg_zw = []
            # 经验分类名
            jy_list = []
            # 最大薪资
            max_xz = []
            # 最小薪资
            min_xz = []
            # 循环遍历存入数据
            returnDate['status'] = 1
            for item in jy_zwyx_list:
                count_zw.append(item[0])
                avg_zw.append(round(item[1], 2))
                jy_list.append({'text': item[2]})
                max_xz.append(int(item[3]))
                min_xz.append(int(item[4]))
            # 数据json化
            returnDate['count_zw'] = count_zw
            returnDate['avg_zw'] = avg_zw
            returnDate['jy_list'] = jy_list
            returnDate['max_xz'] = max_xz
            returnDate['min_xz'] = min_xz
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_jy', rv, timeout=100 * 60)
    return rv


# 职位名称薪资图页面（象形柱图）
# 参考链接：http://echarts.baidu.com/demo.html#pictorialBar-dotted
@app.route('/zwyx/zwmc_index')
def zwyx_zwmc_view():
    return render_template('zwyx_zwmc.html')


# 职位名称和薪资
@app.route('/zwyx/zwmc')
def show_zwyx_zwmc():
    rv = cache.get('zwyx_zwmc')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        returnDate['status'] = 0
        sql = 'select count(zp_list.Id) as count_zw,avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_zwmc.zwmc_name,max(max_zwyx),min(min_zwyx) from zp_list inner join zp_zwmc on zp_zwmc.Id=zp_list.zwmc_id where min_zwyx!=0 group by zp_list.zwmc_id'
        # 执行sql语句
        cursor.execute(sql)
        # 总职位数
        count_zw = {}
        # 平均薪资
        avg_zw = {}
        # 职位名
        zwmc_list = set()
        # 最大薪资
        max_xz = {}
        # 最小薪资
        min_xz = {}
        count_zw['其他'] = []
        avg_zw['其他'] = []
        max_xz['其他'] = 0
        min_xz['其他'] = 0
        zwmc_zwyx_list = cursor.fetchall()
        # 打开职位分类文本
        file = open('zp_list.txt', 'r')
        lines = file.readline().split()
        return_avg_zw = []
        if zwmc_zwyx_list:
            # 提取数据
            for row in zwmc_zwyx_list:
                # 提取文本数据
                for item in lines:
                    # 判断是否包含
                    if item in row[2]:
                        # 如果判断职位列表里是否存在该项
                        if item in zwmc_list:
                            # 存在则处理数据
                            count_zw[item].append(row[0])
                            avg_zw[item].append(row[1])
                            max_xz[item] = max(max_xz[item], row[3])
                            min_xz[item] = min(min_xz[item], row[4])
                        else:
                            # 不存在则添加数据
                            zwmc_list.add(item.decode('utf-8'))
                            count_zw[item] = []
                            count_zw[item].append(row[0])
                            avg_zw[item] = []
                            avg_zw[item].append(row[1])
                            max_xz[item] = row[3]
                            min_xz[item] = row[4]
            return_count_zw = []
            for value in count_zw:
                return_count_zw.append(sum(count_zw[value]))
            for values in avg_zw:
                avg_num_list = avg_zw[values]
                sumAvgNum = sum(avg_num_list)
                if sumAvgNum == 0:
                    avg_num = 0
                else:
                    avg_num = sumAvgNum / len(avg_num_list)
                return_avg_zw.append(round(avg_num, 2))
            # 重新构建数据
            return_max_xz = max_xz.values()
            return_min_xz = min_xz.values()
            # json数据
            returnDate['status'] = 1
            returnDate['count_zw'] = return_count_zw
            returnDate['avg_zw'] = return_avg_zw
            returnDate['zwmc_list'] = list(zwmc_list)
            returnDate['max_xz'] = return_max_xz
            returnDate['min_xz'] = return_min_xz
        else:
            returnDate['status'] = 0
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_zwmc', rv, timeout=100 * 60)
    return rv


# 公司数和地点关系（散点图）
# 参考链接：http://echarts.baidu.com/demo.html#scatter-map-brush
@app.route('/dd/gsmc_index')
def dd_gsmc_view():
    return render_template('dd_gsmc.html')


# 公司数和地点
@app.route('/dd/gsmc')
def show_dd_gsmc():
    rv = cache.get('zwyx_gsmc')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        returnDate['status'] = 0
        sql = 'select count(distinct zp_list.gsmc_id) as count_gs,zp_dd.dd_name from zp_list inner join zp_dd on zp_dd.Id=zp_list.dd_id group by zp_list.dd_id'
        # 执行sql语句
        cursor.execute(sql)
        # 公司数
        count_gs = []
        gsmc_dd_list = cursor.fetchall()
        if gsmc_dd_list:
            returnDate['status'] = 1
            for item in gsmc_dd_list:
                count_gs.append({'name': item[1], 'value': item[0]})
            returnDate['count_gs'] = count_gs
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('zwyx_gsmc', rv, timeout=100 * 60)
    return rv


# 职位数和地点页面（热力图）
# 参考链接：http://echarts.baidu.com/demo.html#heatmap-map
@app.route('/dd/zwmc_index')
def dd_zwmc_view():
    return render_template('dd_zwmc.html')


# 职位名称和地点
@app.route('/dd/zwmc')
def show_dd_zwmc():
    rv = cache.get('dd_zwyx')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        returnDate['status'] = 0
        sql = 'select count(distinct zp_list.zwmc_id) as count_zw,zp_dd.dd_name from zp_list inner join zp_dd on zp_dd.Id=zp_list.dd_id group by zp_list.dd_id'
        # 执行sql语句
        cursor.execute(sql)
        # 职位数
        count_zw = []
        gsmc_dd_list = cursor.fetchall()
        if gsmc_dd_list:
            returnDate['status'] = 1
            for item in gsmc_dd_list:
                count_zw.append({'name': item[1], 'value': item[0]})
            returnDate['count_zw'] = count_zw
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('dd_zwyx', rv, timeout=100 * 60)
    return rv


# 职位类型相关关系（综合图）
# 参考链接：http://echarts.baidu.com/demo.html#watermark
@app.route('/dd/type_index')
def dd_type_view():
    return render_template('dd_type.html')


# 综合图
@app.route('/dd/type')
def show_dd_type():
    rv = cache.get('dd_type')
    if rv is None:
        # 建立链接游标
        cursor = connect_mysql(conn)
        returnDate = {}
        returnDate['status'] = 0
        # 查询职位类型与地点、职位、公司、平均薪资分布总数的关系
        sql = 'SELECT count(distinct zp_list.dd_id),count(distinct zp_list.zwmc_id),count(distinct zp_list.gsmc_id),avg((zp_list.max_zwyx+zp_list.min_zwyx)/2) as avg_zwyx,zp_zwlb.`zwlb_name` from zp_list inner join zp_zwlb ON zp_zwlb.Id =zp_list.zwlb_id where min_zwyx!=0 GROUP BY zp_list.zwlb_id'
        # 执行sql语句
        cursor.execute(sql)
        type_dd_list = cursor.fetchall()
        # 分布地点数
        dd_type = {}
        # 分布地点最大数
        count_dd_type = 0
        # 职位数
        zw_type = {}
        # 公司数
        gs_type = {}
        # 平均薪资
        avg_xz = {}
        # 平均薪资最大数
        max_avg_xz = 0
        # 总平均薪资
        all_avg = 0
        returnDate['status'] = 1
        # 遍历数据
        for item in type_dd_list:
            dd_type[item[4]] = item[0]
            count_dd_type = max(count_dd_type, item[0])
            zw_type[item[4]] = item[1]
            gs_type[item[4]] = item[2]
            avg_xz[item[4]] = round(item[3], 2)
            max_avg_xz = max(max_avg_xz, round(item[3], 2))
            if all_avg == 0:
                all_avg = round(item[3], 2)
            else:
                all_avg = round((all_avg + round(item[3], 2)) / 2, 2)
        # Json化数据
        returnDate['dd_type'] = dd_type
        returnDate['zw_type'] = zw_type
        returnDate['gs_type'] = gs_type
        returnDate['avg_xz'] = avg_xz
        returnDate['all_avg'] = round(all_avg, 2)
        returnDate['count_dd_type'] = count_dd_type + 20
        returnDate['max_avg_xz'] = round(max_avg_xz + 1000.0, 2)
        cursor.close()
        rv = jsonify(returnDate)
        cache.set('dd_type', rv, timeout=100 * 60)
    return rv


# 入口
if __name__ == '__main__':
    # 调试模式
    # app.debug = True
    # 外部可访问的服务器
    app.run(host='0.0.0.0')
